package com.devcamp.devcampdateapi;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class DevcampDateApiApplication {

	public static void main(String[] args) {
		SpringApplication.run(DevcampDateApiApplication.class, args);
	}

}
