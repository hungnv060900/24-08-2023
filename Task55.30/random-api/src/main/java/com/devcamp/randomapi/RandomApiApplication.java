package com.devcamp.randomapi;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class RandomApiApplication {

	public static void main(String[] args) {
		SpringApplication.run(RandomApiApplication.class, args);
		CRandomNumber randomNumber = new CRandomNumber();
		System.out.println(randomNumber.randomDoubleNumber());
		System.out.println(randomNumber.randomIntNumber());
	}

}
