import java.util.ArrayList;

import models.Country;
import models.Region;

public class App {
    public static void main(String[] args) throws Exception {
        //System.out.println("Hello, World!");
        ArrayList<Country> countries = new ArrayList<>();
        
        Country vietnam = new Country("VN", "Vietnam");
        vietnam.addRegion(new Region("SG", "Ho Chi Minh City"));
        vietnam.addRegion(new Region("HN", "Hanoi"));
        
        countries.add(vietnam);
        // Add more countries and regions here
        
        // Print list of countries
        for (Country country : countries) {
            System.out.println("Country: " + country.getCountryName());
            if ("Vietnam".equals(country.getCountryName())) {
                System.out.println("Regions:");
                for (Region region : country.getRegions()) {
                    System.out.println("  " + region.getRegionName());
                }
            }
        }
    }
    
}
