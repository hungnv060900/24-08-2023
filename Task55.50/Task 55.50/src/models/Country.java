package models;

import java.util.ArrayList;

public class Country {
    String countryCode;
    String countryName;
    ArrayList<Region> regions = new ArrayList<>();

    public Country(String countryCode, String countryName) {
        this.countryCode = countryCode;
        this.countryName = countryName;
    }

    public void addRegion(Region region) {
        regions.add(region);
    }

    public ArrayList<Region> getRegions() {
        return regions;
    }
    public String getCountryName() {
        return countryName;
    }
    
}
